function ScreenshotHandler() {
	// Variables
	this.dataUrl = null;

	// Functions
	this.setDeclarativeContentRules = setDeclarativeContentRules;
	this.registerListener = registerListener;
	this.takeCapture = takeCapture;
	this.downloadCapture = downloadCapture;
	this.discardCapture = discardCapture;

	function setDeclarativeContentRules() {
		chrome.declarativeContent.onPageChanged.removeRules(undefined, () => {
      chrome.declarativeContent.onPageChanged.addRules([{
        conditions: [
        	new chrome.declarativeContent.PageStateMatcher({ css: ['video'] }),
        	new chrome.declarativeContent.PageStateMatcher({ css: ['canvas'] })
        ],
        actions: [new chrome.declarativeContent.ShowPageAction()]
      }]);
    });
	}

	function registerListener() {
		chrome.runtime.onMessage.addListener((message, sender, responseCallback) => {
			switch (message.action) {
				case 'capture':
						this.takeCapture(responseCallback);
					break;
				case 'save':
					this.downloadCapture(responseCallback);
					break;
				case 'discard':
					this.discardCapture(responseCallback);
					break;
				default:
					break;
			}
			return true;
		});
	}

	function takeCapture(responseCallback) {
		chrome.tabs.captureVisibleTab(null, { format: 'png' }, (dataUrl) => {
			this.dataUrl = dataUrl;
			responseCallback({ data: { dataUrl } });
		});
	}

	function downloadCapture(responseCallback) {
		if (this.dataUrl) {
			const link = document.createElement('a');
			link.download = 'screenshot.png';
			link.href = this.dataUrl;
			link.click();
			responseCallback({});
			this.dataUrl = null;
		} else {
			responseCallback({ error: new Error('No image data, couldn\'t download') });
			this.dataUrl = null;
		}
	}

	function discardCapture(responseCallback) {
		responseCallback({});
		this.dataUrl = null;
	}

	this.setDeclarativeContentRules();
	this.registerListener();

	return {};
}

const screenshotHandler = new ScreenshotHandler();