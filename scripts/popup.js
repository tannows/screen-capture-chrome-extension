function ScreenshotPreview() {
	// Variables
	this.previewImg = document.getElementById('screenshot-preview');
	this.saveButton = document.getElementById('save-screenshot');
	this.errorDiv = document.getElementById('screenshot-error');

	// Functions
	this.registerListeners = registerListeners;
	this.sendMessage = sendMessage;
	this.takeScreenshot = takeScreenshot;
	this.saveScreenshot = saveScreenshot;

	function registerListeners() {
		this.previewImg.addEventListener('click', () => this.takeScreenshot());
		this.saveButton.addEventListener('click', () => this.saveScreenshot());
	}

	function sendMessage(args) {
		return new Promise((resolve, reject) => {
			chrome.runtime.sendMessage(args, (response) => resolve(response));
		});
	}

	function takeScreenshot() {
		this.errorDiv.textContent = '';
		this.errorDiv.style.display = 'none';
		this.saveButton.disabled = true;
		this.sendMessage({ action: 'capture' }).then((response) => {
			if (response && !response.error && response.data) {
				this.previewImg.src = response.data.dataUrl;
				this.saveButton.disabled = false;
			} else {
				const err = response && response.error ? response.error.message : 'Something went wront';
				this.errorDiv.textContent = err;
				this.errorDiv.style.display = 'block';
			}
		});
	}

	function saveScreenshot() {
		this.errorDiv.textContent = '';
		this.errorDiv.style.display = 'none';
		this.sendMessage({ action: 'save' }).then((response) => {
			if (response && !response.error) {
				window.close();
			} else {
				const err = response && response.error ? response.error.message : 'Something went wront';
				this.errorDiv.textContent = err;
				this.errorDiv.style.display = 'block';
			}
		});
	}

	this.registerListeners();
	this.takeScreenshot();

	return {};
}

var previewer = new ScreenshotPreview();
